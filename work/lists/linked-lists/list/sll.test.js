'use strict';

// node import/export
var List = require('./class/linked-list');




/*
// SLL
var sll = new List.SinglyLinkedList();
sll.append(5);
sll.append(6);
sll.append(7);
console.log('sll:', sll);
// iterate
var it1 = sll[Symbol.iterator]();
it1.next();
it1.next();
// for...of
for (let key of sll) console.log(key);



// DLL
var dll = new List.DoublyLinkedList();
dll.append(29);
dll.append(39);
dll.append(49);
// iterate
var it2 = dll[Symbol.iterator]();
it2.next();
it2.next();
// for...of
for (let key of dll) console.log(key);


// SLCL
var slcl = new List.SinglyLinkedCircularList();
slcl.append(5);
slcl.append(10);
slcl.append(15);
slcl.append(20);
console.log('slcl:', slcl);



// DLCL
var dlcl = new List.DoublyLinkedCircularList();
dlcl.append(25);
dlcl.append(30);
dlcl.append(35);
dlcl.append(40);
console.log('dlcl:', dlcl);
*/


/*
var sll1 = new SinglyLinkedList(1.331, true, "abc", [11,22,33], {z:19, y:91});
var sll2 = new SinglyLinkedList(...[11,22,33]);
var sll = new SinglyLinkedList();
sll.append(55);
sll.prepend(44);
sll.prepend(33);
sll.has(33);
sll.append(77);
sll.prepend(22);
sll.append(99);
sll.prepend(11);
sll.insert(55, 66);
sll.insert(77, 88);
sll.traverse();
sll.traverse(x => x + 2);

var sll3 = new SinglyLinkedList();
console.log('sll3.append(55): ', sll3.append(55));
console.log('sll3.prepend(44): ', sll3.prepend(44));
console.log('sll3.prepend(33): ', sll3.prepend(33));
console.log('sll3.has(33): ', sll3.has(33));
console.log('sll3.append(77): ', sll3.append(77));
console.log('sll3.prepend(22): ', sll3.prepend(22));
console.log('sll3.append(99): ', sll3.append(99));
console.log('sll3.prepend(11): ', sll3.prepend(11));
console.log('sll3.insert(55, 66): ', sll3.insert(55, 66));
console.log('sll3.insert(77, 88): ', sll3.insert(77, 88));
console.log('sll3.traverse(): ', sll3.traverse());
*/
