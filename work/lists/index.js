'use strict';

// node import/export
var List = require('./class/linked-list');




/*
// SLL
var sll = new List.SinglyLinkedList();
sll.append(5);
sll.append(6);
sll.append(7);
console.log('sll:', sll);
// iterate
var it1 = sll[Symbol.iterator]();
it1.next();
it1.next();
// for...of
for (let key of sll) console.log(key);



// DLL
var dll = new List.DoublyLinkedList();
dll.append(29);
dll.append(39);
dll.append(49);
// iterate
var it2 = dll[Symbol.iterator]();
it2.next();
it2.next();
// for...of
for (let key of dll) console.log(key);


// SLCL
var slcl = new List.SinglyLinkedCircularList();
slcl.append(5);
slcl.append(10);
slcl.append(15);
slcl.append(20);
console.log('slcl:', slcl);



// DLCL
var dlcl = new List.DoublyLinkedCircularList();
dlcl.append(25);
dlcl.append(30);
dlcl.append(35);
dlcl.append(40);
console.log('dlcl:', dlcl);
*/