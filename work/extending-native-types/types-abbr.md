# types

boolean      b       bn      boo
number       n       nr      num
string       s       sg      str
symbol       y       sy      sym
null         0       nl      nul
undefined    u       un      und
Object          ob      obj
Function        fn      fun
Array           ar      arr
Map             ma      map
Set             se      set
WeakMap         wm      wma
WeakSet         ws      wse
RegExp          re      reg
Date            da      dat
JSON            js      jsn
Iterator            it      ite
Generator           ge      gen
GeneratorFunction   gf      gfn
AsyncFunction       af      afn
Reflect             rf      ref
Proxy               px      pxy
Promise             pr      prm

er: Error
te: TypeError
    RangeError
    ReferenceError
se: SyntaxError
ee: EvalError
ie: InternalError
ui: URIError

mt: Math
ag: arguments
in: Intl
wa: WebAssembly
at: Atomics
si: SIMD

ta: [TypedArray]
i8: Int8Array
u8: Uint8Array
c8: Uint8ClampedArray
    Int16Array
    Uint16Array
    Int32Array
    Uint32Array
    Float32Array
    Float64Array
ab: ArrayBuffer
dw: DataView
sb: SharedArrayBuffer







1 object named `Off`

- general function in Off.fn()
- functions realted to arrays on Off.array.fn()
- Off aliased as $


Methods:

- `head` (aliased as `peak`)
- `tail` (aliased as `last`)
- `range`
- `flatten`
- `pack`
- `collapseOne` (aliased as `concatAll`)
