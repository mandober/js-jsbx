

Number.NEGATIVE_INFINITY;

-0;
0;
Number.EPSILON;

Number.MIN_SAFE_INTEGER;    // -2**53-1
Number.MIN_VALUE;           // 5.00e-324

Number.MAX_SAFE_INTEGER;    // 2**53-1
Number.MAX_VALUE;           // 1.79e+308

Number.POSITIVE_INFINITY;

