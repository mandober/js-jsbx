//* Chapter 3: Managing Function Inputs

//* arity

// function that expects multiple arguments is broken down into successive chained functions that each take a single argument (arity: 1) and return another function to accept the next argument.

// currying unwinds a higher-arity function into a series of chained unary functions.




//! getArgs
const arity = function arity(fn) {
    return fn.toString()
    .replace(/^(?:(?:function.*\(([^]*?)\))|(?:([^\(\)]+?)\s*=>)|(?:\(([^]*?)\)\s*=>))[^]+$/, "$1$2$3")
    .split(/\s*,\s*/)
    .map(v => v.replace(/[=\s].*$/, ""));
};

arity(foo); /*?*/
arity(partial2); /*?*/
arity(partial); /*?*/
arity(ajax); /*?*/
arity(jax); /*?*/
arity(jax1); /*?*/
arity(reverseArgs); /*?*/
arity(partialRight); /*?*/
arity(arity); /*?*/

