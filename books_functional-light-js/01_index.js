//* Chapter 2: Foundations of Functional Functions

//* arity
// The number of function's declared parameters
// accessible via: fn.length
function foo(x, y, z) {foo.length;} // 3
// with some exceptions:
// 1) default parameters
function foo(x, y = 2) { foo.length; } // 1
// 2) rest
function bar(x, ...args) { foo.length; } // 1
// 3) destructuring
function baz({ a, b }) { foo.length; } // 1


//* arrow functions
/*
- lexical this
- lexical arguments
- its name can only be infered, so no self-referencing
- multuple statements (blocks) introduce curly-braces thus `return` must be stated explicitly
- rules when parens are needed:
*/
people.map(person => person.nicknames[0] || person.firstName);

//? multiple parameters
people.map((person, idx) => person.nicknames[0] || person.firstName);

//? parameter destructuring
people.map(({ person }) => person.nicknames[0] || person.firstName);

//? parameter defaults
people.map((person = {}) => person.nicknames[0] || person.firstName);

//? returning an object
people.map(person =>
    ({ preferredName: person.nicknames[0] || person.firstName })
);
