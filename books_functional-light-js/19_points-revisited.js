//! Revisiting Points
//* see composition in action by revisiting point-free style with a scenario that's complex to refactor:

// given: ajax( url, data, cb )
var getPerson = partial(ajax, "http://some.api/person");
var getLastOrder = partial(ajax, "http://some.api/order", { id: -1 });

getLastOrder(function orderFound(order) {
    getPerson({ id: order.personId }, function personFound(person) {
        output(person.name);
    });
});

//* The "points" we'd like to remove are the `order` and `person` parameter references.

//* Let's start by trying to get the person "point" out of the personFound(..) function.
// To do so, let's first define:
function extractName2(person) {
    return person.name;
}

//* But let's observe that this operation could instead be expressed in generic terms:
// extracting any property by name off of any object.
//! getProp()
const getProp = (name, obj) => obj[name];

//* While we're dealing with object properties, let's also define the opposite utility:
// setProp(..) for setting a property value onto an object.
// However, we want to be careful not to just mutate an existing object but rather create a (shallow) clone of the object to make the change to, and then return it.
//! setProp()
const setProp = (name, obj, val) => {
    //! WARNING: shallow clone!
    var o = Object.assign({}, obj);
    o[name] = val;
    return o;
};

//* now, to define an extractName(..) that pulls a "name" property off an object, we'll partially apply prop(..):
var extractName = partial(getProp, "name");

// extractName(..) here hasn't actually extracted anything yet.
// We partially applied prop(..) to make a function that's waiting to extract the "name" property
// from whatever object we pass into it. We could also have done it with curry(prop)("name").

//* Next, let's narrow the focus on our example's nested lookup calls to this:
getLastOrder(function orderFound(order) {
    getPerson({ id: order.personId }, outputPersonName);
});

/*
How can we define `outputPersonName()`?
To visualize what we need, think about the desired flow of data:

output <-- extractName <-- person

`outputPersonName()` needs to be a function that takes an (object) value,
passes it into `extractName()`, then passes that value to `output()`.

Hopefully you recognized that as a `compose()` operation.
So we can define `outputPersonName()` as:
*/
var outputPersonName = compose( output, extractName );

// The outputPersonName(..) function we just created is the callback provided to getPerson(..).So we can define a function called processPerson(..) that presets the callback argument, using partialRight(..):
var processPerson = partialRight(getPerson, outputPersonName);

// Let's reconstruct the nested lookups example again with our new function:
getLastOrder(function orderFound(order) {
    processPerson({ id: order.personId });
});


// But we need to keep going and remove the order "point". The next step is to observe that personId can be extracted from an object (like order) via prop(..), just like we did with name on the person object:
var extractPersonId = partial(prop, "personId");


// To construct the object (of the form { id: .. }) that needs to be passed to processPerson(..), let's make another utility for wrapping a value in an object at a specified property name, called makeObjProp(..).
// This utility is known as `objOf()` in the Ramda library.
//! makeObjProp()
const makeObjProp = (name, value) =>
    setProp(name, {}, value);


// Just as we did with prop(..) to make extractName(..), we'll partially apply makeObjProp(..) to build a function personData(..) that makes our data object:
var personData = partial(makeObjProp, "id");

/*
to use processPerson(..) to perform the lookup of a person attached to an order value,
the conceptual flow of data through operations we need is:

processPerson < --personData < --extractPersonId < --order

So we'll just use compose(..) again to define a lookupPerson(..) utility:
*/
var lookupPerson = compose(processPerson, personData, extractPersonId);


/// And... that's it! Putting the whole example back together without any "points":
var getPerson        = partial(ajax, "http://some.api/person");
var getLastOrder     = partial(ajax, "http://some.api/order", { id: -1 });
var extractName      = partial(getProp, "name");
var outputPersonName = compose(output, extractName);
var processPerson    = partialRight(getPerson, outputPersonName);
var personData       = partial(makeObjProp, "id");
var extractPersonId  = partial(prop, "personId");
var lookupPerson     = compose(processPerson, personData, extractPersonId);
getLastOrder(lookupPerson);


// And even if you didn't like seeing/naming all those intermediate steps, you can preserve point-free but wire the expressions together without individual variables:
partial(ajax, "http://some.api/order", { id: -1 })
    (
    compose(
        partialRight(
            partial(ajax, "http://some.api/person"),
            compose(output, partial(prop, "name"))
        ),
        partial(makeObjProp, "id"),
        partial(prop, "personId")
    )
    );

// This snippet is less verbose for sure, but I think it's less readable than the previous snippet where each operation is its own variable. Either way, composition helped us with our point-free style.

