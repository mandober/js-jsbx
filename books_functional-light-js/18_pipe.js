//* pipe
/*
We talked earlier about the right-to-left ordering of standard compose(..) implementations.

The advantage is in listing the arguments (functions) in the
same order they'd appear if doing the composition manually.

The disadvantage is they're listed in the reverse order that they execute, which could be confusing.
It was also more awkward to have to use partialRight(compose, ..) to pre-specify the first function(s)
to execute in the composition.

The reverse ordering, composing from left-to-right, has a common name: pipe().
pipe() is identical to compose() except it processes through the list of functions in left-to-right order.
*/
function pipe22(...fns) {
    return function piped(result) {
        var list = fns.slice();
        while (list.length > 0) {
            // take the first function from the list and execute it
            result = list.shift()(result);
        }
        return result;
    };
}

//! pipe
const pipe = (...fns) =>
    (result) => {
        let list = fns.slice();
        while (list.length > 0) {
            // take the first function from the list and execute it
            result = list.shift()(result);
        }
        return result;
    };


// In fact, we could just define pipe() as the arguments-reversal of compose():
const reverseArgs = (fn) =>
    (...args) =>
        fn(...args.reverse());

//! pipe2
const pipe2 = reverseArgs(compose);
