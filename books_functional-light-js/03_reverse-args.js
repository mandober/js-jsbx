//* Chapter 3: Managing Function Inputs

//? Reversing Arguments

//! reverseArgs
// Recall that the signature for our Ajax function is: ajax( url, data, cb).What if we wanted to partially apply the cb but wait to specify data and url later? We could create a utility that wraps a function to reverse its argument order:
const reverseArgs = (fn) =>
    (...args) =>
        fn(...args.reverse());


// Now we can reverse the order of the ajax(..) arguments, so that we can then partially apply from the right rather than the left.To restore the expected order, we'll then reverse the partially applied function:
var cache = {};

var cacheResult = reverseArgs(
    partial(reverseArgs(ajax), function onResult(obj) {
        cache[obj.id] = obj;
    })
);

cacheResult("http://some.api/person", { user: CURRENT_USER_ID });
