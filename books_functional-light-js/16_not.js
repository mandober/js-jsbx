// a not(..) negation helper (often referred to as complement(..) in FP libraries):

//! not
var not = predicate =>
    (...args) =>
        !predicate(...args);

// Next, let's use not(..) to alternately define isLongEnough(..) without "points":
var isLongEnough = not(isShortEnough);
printIf(isLongEnough, msg2); // Hello World


// We can express the if conditional part with a when(..) utility:
//! when
var when = (predicate, fn) =>
    (...args) =>
        predicate(...args) ? fn(...args) : undefined;


// Let's mix when(..) with a few other helper utilities we've seen earlier in this chapter, to make the point- free printIf(..):
var printIf = uncurry(rightPartial(when, output));





// Here's the whole example put back together (assuming various utilities we've already detailed in this chapter are present):
function output(msg) {
    console.log(msg);
}

function isShortEnough(str) {
    return str.length <= 5;
}

var isLongEnough = not(isShortEnough);

var printIf = uncurry(partialRight(when, output));

var msg1 = "Hello";
var msg2 = msg1 + " World";

printIf(isShortEnough, msg1); // Hello
printIf(isShortEnough, msg2);

printIf(isLongEnough, msg1);
printIf(isLongEnough, msg2); // Hello World
