//* Chapter 3: Managing Function Inputs

//! partial
const partial =
    (fn, ...presetArgs) =>
        (...laterArgs) =>
            fn(...presetArgs, ...laterArgs); /*?*/


// examples:
var sum = (a, b, c) => a + b + c;
var add = (fn, x, y, z) => fn(x, y, z);

var addition = partial(sum, 2); //?
addition(3, 5); //?

partial(sum)(2)(3)(5); //?
partial(sum)(2, 3, 5); //?
partial(sum, 2)(3, 5); //?
partial(sum)(2)(3, 5); //?



// examples:
var ajax = (url, data, callback) => [url, data, callback];
var ajax1 = partial(ajax, "www.ABC.com");
var [url, data, callback] = ajax1("data", console.log); //?

var jax = (url) => [url];
var jax1 = partial(jax, 1223);
var [url] = jax1(8, 9); //?
