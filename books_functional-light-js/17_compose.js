//! compose1
const compose1 = (...fns) =>
    result => {
        var list = fns.slice();

        while (list.length > 0) {
            // take the last function off the end of the list and execute it
            result = list.pop()(result);
        }

        return result;
    };

//! compose2
// another implementation with reduce
const compose2 = (...fns) =>
    result =>
        fns.reverse()
           .reduce((acc, fn) => fn(acc), acc);


// However, this implementation is limited in that the outer composed function
// (aka, the first function in the composition) can only receive a single argument.
// Most other implementations pass along all arguments to that first call.
// If every function in the composition is unary, this is no big deal. But if you need
// to pass multiple arguments to that first call, you'd want a different implementation.

// To fix that first call single-argument limitation, we can still
// use reduce(..) but produce a lazy-evaluation function wrapping:
//! compose3
const compose3 = (...fns) =>
    fns.reverse()
       .reduce( (acc, fn) => (...args) => fn( acc(...args) ) );

/*
Notice that we return the result of the reduce() call directly,
which is itself a function, not a computed result.

That function lets us pass in as many arguments as we want, passing them all down the line to the
first function call in the composition, then bubbling up each result through each subsequent call.

Instead of calculating the running result and passing it along as the reduce() looping
proceeds, this implementation runs the reduce() looping once up front at composition time,
and defers all the function call calculations -- referred to as lazy calculation.

Each partial result of the reduction is a successively more wrapped function.

When you call the final composed function and provide one or more arguments,
all the levels of the big nested function, from the inner most call to the outer,
are executed in reverse succession (not via a loop).

The performance characteristics will potentially be different than in the previous reduce-based implementation.
Here, reduce() only runs once to produce a big composed function, and then this composed function call simply
executes all its nested functions each call. In the former version, reduce() would be run for every call.
*/

//* recursive compose
// We could also define compose(..) using recursion.
// The recursive definition for `compose(f1, f2, ..., fn)` would look like:
// compose(compose(f1, f2, ... fn-1), fn);

//! compose4
const compose4 = (...fns) => {
    // pull off the last two arguments i.e. functions
    let [fn1, fn2, ...rest] = fns.reverse();

    const composedFn = (...args) => fn2(fn1(...args));

    return (rest.length == 0)
        // if there are only 2 fns, return `composedFn` function
        // that will apply composure of the 2 fns onto its upcoming args
        ? composedFn
        // if more remain, call self recursively with remaining fns reversed
        // and the `composedFn` function
        : compose4(...rest.reverse(), composedFn);
};


