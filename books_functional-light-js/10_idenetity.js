// function that takes one argument and does nothing but return the value untouched:

//! identity
const identity = v => v;

// For example, imagine you'd like split up a string using a regular expression, but the resulting array may have some empty values in it. To discard those, we can use JS's filter(..) array operation with identity(..) as the predicate:
var words = "   Now is the time for all...  ".split(/\s|\b/);
words; // ["","Now","is","the","time","for","all","...",""]

words.filter(identity);
// ["Now","is","the","time","for","all","..."]



// Another example of using identity(..) is as a default function in place of a transformation:
function output(msg, formatFn = identity) {
    msg = formatFn(msg);
    console.log(msg);
}

function upper(txt) {
    return txt.toUpperCase();
}

output("Hello World", upper);		// HELLO WORLD
output("Hello World");			// Hello World


// If output(..) didn't have a default for formatFn, we could bring our earlier friend partialRight(..):
var specialOutput = partialRight(output, upper);
var simpleOutput = partialRight(output, identity);
specialOutput("Hello World"); // HELLO WORLD
simpleOutput("Hello World");  // Hello World

//* identity(..) can also be used as a default transformation function for map(..) calls or
//* as the initial value in a reduce(..) of a list of functions

