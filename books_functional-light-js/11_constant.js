//* constant

// Certain APIs don't let you pass a value directly into a method, but require you to pass in a function, even if that function just returns the value. One such API is the then(..) method on JS Promises.

//! kestrel (constant fn maker)
var constant = (v) => () => v;


// With this tidy little utility, we can solve our then(..) annoyance:
p1.then(foo).then(() => p2).then(bar);
// vs
p1.then(foo).then(constant(p2)).then(bar);

// Although the `() => p2` arrow function version is shorter than constant(p2), the arrow function is returning a value from outside of itself, which is a bit worse from the FP perspective.
