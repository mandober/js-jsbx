function maths(cb, x, y, z) {
    return cb(x, y, z);
}
var sum = (a, b, c) => a + b + c;
maths(sum, 30, 5, 2); /*?*/


const getArity = require('../off/getArity/');

function partial(fn, ...now) {
    // check if fn is fn
    if (typeof fn === "function") {
        // get its arity
        var argc = getArity(fn);
    } else {
        return new Error("First arg must be a function");
    }

    if (argc <= now.length) {
        return fn(...now);
    } else {
        return function (...later) {
            return partial(fn, ...now, ...later)
        }
    }
}



// var addition = partial(sum, 6, 3, 8); /*?*/

// var addition2 = partial(sum, 6, 3); /*?*/
// addition2(6); /*?*/

var addition1 = partial(sum); /*?*/
// var addition2 = addition1(6, 7); /*?*/
// addition2(4); /*?*/

var addition3 = addition1(6); /*?*/
var addition4 = addition3(3); /*?*/
addition4(2); /*?*/




/*
var addition = partial(sum);
addition(10, 2);
var addition6 = partial(sum, 6);
addition6(5);
addition6(8);
*/
