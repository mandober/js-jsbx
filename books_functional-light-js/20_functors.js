//! Functors
//* functor is a value that has a utility for using an operator function on that value.

// In JS array is a functor

//* If the value in question is compound, meaning it's comprised of individual values -- as is the case with arrays, for example! -- a functor uses the operator function on each individual value. Moreover, the functor utility creates a new compound value holding the results of all the individual operator function calls.

// a string functor would be a string plus a utility that executes some operator function across all the characters in the string, returning a new string with the processed letters.

//! map
function map(mapperFn, arr) {
    var newList = [];
    for (let idx = 0; idx < arr.length; idx++) {
        newList.push(
            mapperFn(arr[idx], idx, arr)
        );
    }
    return newList;
}

// Note: The parameter order mapperFn, arr may feel backwards at first, but this convention is much
// more common in FP libraries because it makes these utilities easier to compose (with currying).
// The mapperFn(..) is naturally passed the list item to map/transform, but also an idx and arr.
// We're doing that to keep consistency with the built-in array map().


//! filter (= filterIn)
function filter(predicateFn, arr) {
    var newList = [];
    for (let idx = 0; idx < arr.length; idx++) {
        if (predicateFn(arr[idx], idx, arr)) {
            newList.push(arr[idx]);
        }
    }
    return newList;
}

// Notice that just like mapperFn() before, predicateFn() is passed not only the value but also the idx and arr.
// Use unary() to limit its arguments as necessary.

//! predicate function
// A predicate function is the one that returns a boolean value.


//? filter-out & filter-in
// To clear up all this confusion, let's define a filterOut() that actually filters out values by internally negating the predicate check. While we're at it, we'll alias filterIn() to the existing filter():

//! filterIn()
var filterIn = filter;

//! filterOut()
function filterOut(predicateFn, arr) {
    return filterIn(not(predicateFn), arr);
}

// Now we can use whichever filtering makes most sense at any point in our code:
isOdd(3); // true
isEven(2); // true
filterIn(isOdd, [1, 2, 3, 4, 5]); // [1,3,5]
filterOut(isEven, [1, 2, 3, 4, 5]); // [1,3,5]

// I think using `filterIn` and `filterOut`, known as `reject` in Ramda,
// will make your code a lot more readable than just using `filter`
// and leaving the semantics conflated and confusing for the reader.





//! reduce

// A standalone implementation of reduce() might look like this:
function reduce(reducerFn, initialValue, arr) {
    var acc, startIdx;

    if (arguments.length == 3) {
        acc = initialValue;
        startIdx = 0;
    }

    else if (arr.length > 0) {
        acc = arr[0];
        startIdx = 1;
    }

    else {
        throw new Error("Must provide at least one value.");
    }

    for (let idx = startIdx; idx < arr.length; idx++) {
        acc = reducerFn(acc, arr[idx], idx, arr);
    }

    return acc;
}

//! binary function
var binary = fn =>
    (arg1, arg2) =>
        fn(arg1, arg2);



//* Map As Reduce
// The map() operation is iterative in its nature, so it can also be represented as a reduction (reduce).
// The trick is to realize that the initialValue of reduce() can be itself an (empty) array,
// in which case the result of a reduction can be another list. Also, mind the comma operator.
var double = v => v * 2;
[1, 2, 3, 4, 5].map(double); // [2,4,6,8,10]
[1, 2, 3, 4, 5].reduce( ((list, v) => list.push(double(v)), list), [] ); // [2,4,6,8,10]


//* Filter As Reduce
// Just as map(..) can be done with reduce(..), so can filter(..):
var isOdd = v => v % 2 == 1;
[1, 2, 3, 4, 5].filter(isOdd);// [1,3,5]
[1, 2, 3, 4, 5].reduce((list, v) => isOdd(v) ? list.push(v) : (undefined, list), []); // [1,3,5]
// More impure reducer cheating here.Instead of list.push(),
// we could have done list.concat() and returned the new list.

