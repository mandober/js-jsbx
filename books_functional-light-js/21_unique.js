//! Unique
// Filtering a list to include only unique values, based on indexOf() searching
// ( which uses === strict equality comparision):
var unique = (arr) =>
        arr.filter(
            (v, idx) =>
                arr.indexOf(v) == idx
        );
