// mapper(..) and double(..) have the same (or compatible, anyway) signatures
function double(x) {
    return x * 2;
}

[1, 2, 3, 4, 5].map(function mapper(v) {
    return double(v);
}); // [2,4,6,8,10]


// The parameter ("point") v can directly map to the corresponding argument in the double(..) call. As such, the mapper(..) function wrapper is unnecessary. Let's simplify with point-free style:
function double(x) {
    return x * 2;
}

[1, 2, 3, 4, 5].map(double); // [2,4,6,8,10]

// Let's revisit an example from earlier:
["1", "2", "3"].map(function mapper(v) {
    return parseInt(v);
}); // [1,2,3]

// In this example, mapper(..) is actually serving an important purpose, which is to discard the index argument that map(..) would pass in, because parseInt(..) would incorrectly interpret that value as a radix for the parsing. This was an example where unary(..) helps us out:
["1", "2", "3"].map(unary(parseInt)); // [1,2,3]

