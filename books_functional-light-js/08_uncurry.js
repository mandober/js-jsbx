//! uncurry

//* uncurry(curry(f)) does not have the same behavior as f
// The uncurried function acts (mostly) the same as the original function if you pass as many arguments to it as the original function expected. However, if you pass fewer arguments, you still get back a partially curried function waiting for more arguments.

var uncurry = fn =>
    (...args) => {
        var ret = fn;

        for (let i = 0; i < args.length; i++) {
            ret = ret(args[i]);
        }

        return ret;
    };
