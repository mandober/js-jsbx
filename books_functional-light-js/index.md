# Functional-Light JS by Kyle Simpson
A book about functional programming in JavaScript.
https://github.com/getify/Functional-Light-JS


## Table of Contents

* [Foreword](https://github.com/getify/Functional-Light-JS/blob/master/foreword.md)
* [Preface](https://github.com/getify/Functional-Light-JS/blob/master/preface.md)

* Chapter 1: [Why Functional Programming](https://github.com/getify/Functional-Light-JS/blob/master/ch1.md)
* Chapter 2: [Foundations of Functional Functions](https://github.com/getify/Functional-Light-JS/blob/master/ch2.md)
* Chapter 3: [Managing Function Inputs](https://github.com/getify/Functional-Light-JS/blob/master/ch3.md)
- Chapter 4: [Composing Functions](https://github.com/getify/Functional-Light-JS/blob/master/ch4.md)
- Chapter 5: [Reducing Side Effects](https://github.com/getify/Functional-Light-JS/blob/master/ch5.md)
- Chapter 6: [Value Immutability](https://github.com/getify/Functional-Light-JS/blob/master/ch6.md)
- Chapter 7: [Closure vs Object](https://github.com/getify/Functional-Light-JS/blob/master/ch7.md)
- Chapter 8: [List Operations](https://github.com/getify/Functional-Light-JS/blob/master/ch8.md)
- Chapter 9: [Recursion](https://github.com/getify/Functional-Light-JS/blob/master/ch9.md)
- Chapter 10: [Functional Async](https://github.com/getify/Functional-Light-JS/blob/master/ch10.md)
- Chapter 11: [Putting It All together](https://github.com/getify/Functional-Light-JS/blob/master/ch11.md)

* Appendix A: Transducing
* Appendix B: The Humble Monad
* Appendix C: FP Libraries
