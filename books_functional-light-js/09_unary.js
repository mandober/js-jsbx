//* unary

// Imagine you're passing a function to a utility where it will send multiple arguments to your function. But you may only want to receive a single argument. This simple utility wraps a function call to ensure only one argument will pass through, effectively enforcing a function is treated as unary function.

//! unary
var unary = fn =>
    arg =>
        fn(arg);


// common example is `parseInt(str,[radix])` that expectes optinal 2nd arg (radix).
["1", "2", "3"].map(parseInt);
// [1,NaN,NaN]
["1", "2", "3"].map(unary(parseInt));
// [1,2,3]

