// Spreading Properties

// Unfortunately, this only works because we have control over the signature of foo(..) and defined it to destructure its first parameter. What if we wanted to use this technique with a function that had its parameters indivdually listed (no parameter destructuring!), and we couldn't change that function signature?

// Just like the spreadArgs(..) utility earlier, we could define a spreadArgProps(..) helper that takes the key: value pairs out of an object argument and "spreads" the values out as individual arguments.

// There are some quirks to be aware of, though. With spreadArgs(..), we were dealing with arrays, where ordering is well defined and obvious. However, with objects, property order is less clear and not necessarily reliable. Depending on how an object is created and properties set, we cannot be absolutely certain what enumeration order properties would come out.

// Such a utility needs a way to let you define what order the function in question expects its arguments (e.g., property enumeration order). We can pass an array like ["x", "y", "z"] to tell the utility to pull the properties off the object argument in exactly that order.

// That's decent, but it's also unfortunate that we kinda have to do add that property-name array even for the simplest of functions. Is there any kind of trick we could use to detect what order the parameters are listed for a function, in at least the common simple cases? Fortunately, yes!

//! spreadArgProps
function spreadArgProps(
    fn,
    propOrder =
        fn.toString()
            .replace(/^(?:(?:function.*\(([^]*?)\))|(?:([^\(\)]+?)\s*=>)|(?:\(([^]*?)\)\s*=>))[^]+$/, "$1$2$3")
            .split(/\s*,\s*/)
            .map(v => v.replace(/[=\s].*$/, ""))
) {
    return function spreadFn(argsObj) {
        return fn(...propOrder.map(k => argsObj[k]));
    };
}


// Let's illustrate using our spreadArgProps(..) utility:
function bar(x, y, z) {
    console.log(`x:${x} y:${y} z:${z}`);
}

var f3 = curryProps(spreadArgProps(bar), 3);
var f4 = partialProps(spreadArgProps(bar), { y: 2 });
f3({ y: 2 })({ x: 1 })({ z: 3 }); // x:1 y:2 z:3
f4({ z: 3, x: 1 }); // x:1 y:2 z:3
