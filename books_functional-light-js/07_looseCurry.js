//! looseCurry

//* This version allows more than 1 arg at the time

function looseCurry(fn, arity = fn.length) {
    return (function nextCurried(prevArgs) {
        return function curried(...nextArgs) {
            var args = prevArgs.concat(nextArgs);
            return (args.length >= arity)
                ? fn(...args)
                : nextCurried(args);
        };
    })([]);
}

//! curry()
//* looser variant
const curry = (fn, arity = fn.length) =>
    (nextCurried = (prevArgs) =>
        (...nextArgs) => {
            var args = prevArgs.concat(nextArgs);
            return (args.length >= arity)
                ? fn(...args)
                : nextCurried(args);
        }
    )([]);


// example
var sum = (a, b, c) => a + b + c;
//* initially, curry must be called with just 1 arg (i.e. the fn)
//* initially, curry cannot be called with 2 args (e.g. a fn and a number)
var add = curry(sum); //?
//* after that formal params can be filled individually or grouped:
// strict curry:
add(2)(5)(7); //?
// loose curry:
add(2, 5, 7); //?
add(2)(5, 7); //?
add(2, 5)(7); //?
