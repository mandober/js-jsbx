//* Chapter 3: Managing Function Inputs: curring

// Currying is similar to partial application in that each successive curried call kind of partially applies another argument to the original function, until all arguments have been passed. The main difference is that curry returns a function that expects only the next argument.

function curry2(fn, arity = fn.length) {
    return (function nextCurried(prevArgs) {
        return function curried(nextArg) {
            var args = prevArgs.concat([nextArg]);

            if (args.length >= arity) {
                return fn(...args);
            }
            else {
                return nextCurried(args);
            }
        };
    })([]);
}


//! curry()
//const getArity = require('../off/get-arity/');

const curry = (fn, arity = fn.length, nextCurried) =>
    (nextCurried = (prevArgs) =>
        (nextArg) => {
            var args = prevArgs.concat([nextArg]); /*? prevArgs */
            return (args.length >= arity) /*? nextArg */
                ? fn(...args) /*? args */
                : nextCurried(args); /*? args */
        } /*? prevArgs */
    )([]);

// example 1
var ajax = (url, data, callback) => [url, data, callback];
var curriedAjax = curry(ajax); //?
var personFetcher = curriedAjax("http://some.api/person"); //?
var getCurrentUser = personFetcher({ user: 1 }); //?
getCurrentUser((user) => console.log("user is", user)); //?


// example 2
var sum = (a, b, c) => a+b+c;
var add = curry(sum); //?
var sum2 = add(2); //?
var sum6 = sum2(4); //?
var res = sum6(5); //?
// add(5)(6)(3); //?



// example 3
var adder = (x, y) => x + y;
[1, 2, 3, 4, 5].map(curry(adder)(3)); //?
// [4,5,6,7,8]



// example 4
var add4 = curry(adder)(4);
[1, 2, 3, 4, 5].map(add4); //?



// example 5
const summer = (...args) => args.reduce((acc, el) => acc + el, 0);
summer(1, 2, 3, 4, 5); //?
var curriedSum = curry(summer, 5); //?
curriedSum(1)(2)(3)(4)(5); //?


