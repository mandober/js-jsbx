//* Chapter 3: Managing Function Inputs
//* partialRight

//! reverseArgs
var reverseArgs = (fn) =>
    (...args) =>
        fn(...args.reverse());

//! partial
const partial = (fn, ...presetArgs) =>
    (...laterArgs) =>
        fn(...presetArgs, ...laterArgs);

// Now, we can define a partialRight(..) which partially applies from the right,
// using this same reverse-partial apply-reverse trick:
//! partialRight()
const partialRight = (fn, ...presetArgs) =>
    reverseArgs(
        partial(reverseArgs(fn), ...presetArgs.reverse())
    );



var cacheResult = partialRight(ajax, function onResult(obj) {
    cache[obj.id] = obj;
});

// later:
cacheResult("http://some.api/person", { user: CURRENT_USER_ID });


// This implementation of partialRight() does not guarantee that a specific parameter will receive a specific
// partially-applied value; it only ensures that the right-partially applied value(s) appear as the right-most
// argument(s) passed to the original function.

// For example:
function foo(x, y, z) {
    var rest = [].slice.call(arguments, 3);
    console.log(x, y, z, rest);
}

var f = partialRight(foo, "z:last");

f(1, 2); // 1 2 "z:last" []
f(1); // 1 "z:last" undefined []
f(1, 2, 3); // 1 2 3 ["z:last"]
f(1, 2, 3, 4); // 1 2 3 [4,"z:last"]

// The value "z:last" is only applied to the z parameter in the case where f() is called with exactly two arguments (matching x and y parameters). In all other cases, the "z:last" will just be the right-most argument, however many arguments precede it.
