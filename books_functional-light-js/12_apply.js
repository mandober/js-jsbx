//* helper to adapt a function so that it spreads out a single received array as its individual arguments
function spreadArgs(fn) {
    return function spreadFn(argsArr) {
        return fn(...argsArr);
    };
}

//! apply
var apply =
    fn =>
        argsArr =>
            fn(...argsArr);


// a utility to handle the opposite action:
function gatherArgs(fn) {
    return function gatheredFn(...argsArr) {
        return fn(argsArr);
    };
}

//! unapply
var unapply =
    fn =>
        (...argsArr) =>
            fn(argsArr);
