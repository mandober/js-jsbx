# JS Types

1.undefined
2.null
3.boolean / Boolean
4.string  / String
5.symbol  / Symbol
6.number  / Number

- object

07.Object (pojo)
08.Array
09.Function
10.Map
11.Set

WeakMap
WeakSet
Date
RegExp
JSON
Reflect
Proxy
Promise
Iterator
Generator
GeneratorFunction
AsyncFunction
arguments
Math
StopIteration
Atomics
DataView
ArrayBuffer
SharedArrayBuffer
ParallelArray


**Errors**
Error
EvalError
InternalError
RangeError
ReferenceError
SyntaxError
TypeError
URIError


**Indexed collections**
Int8Array
Uint8Array
Uint8ClampedArray
Int16Array
Uint16Array
Int32Array
Uint32Array
Float32Array
Float64Array


**Vector collections**
SIMD
SIMD.Float32x4
SIMD.Float64x2
SIMD.Int8x16
SIMD.Int16x8
SIMD.Int32x4
SIMD.Uint8x16
SIMD.Uint16x8
SIMD.Uint32x4
SIMD.Bool8x16
SIMD.Bool16x8
SIMD.Bool32x4
SIMD.Bool64x2

**Internationalization****
Intl
Intl.Collator
Intl.DateTimeFormat
Intl.NumberFormat

**WebAssembly**
WebAssembly
WebAssembly.Module
WebAssembly.Instance
WebAssembly.Memory
WebAssembly.Table
WebAssembly.CompileError
WebAssembly.LinkError
WebAssembly.RuntimeError

