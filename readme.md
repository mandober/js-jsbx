# JSBX

*name*: `js-jsbx`
*repo*: `https://bitbucket.org/mandober/js-jsbx`



# Explore

- mutating objects: instead of deep copy, mutate obj by retaining original obj and just log the differences
- macros: use lazy evaluation of function's body, or || and && operators; parse with regex


## Types

