// ! Groups

let groups = {};
groups.falsey = new Set([undefined, NaN, "", null, false, 0]);
groups.nothing = new Set([undefined, NaN, "", null]);
// groups.empty = new Set([ undefined, NaN, "", null, [], {}, (() => ) ]);
groups.primitives = new Set(["null", "undefined", "boolean", "number", "string", "symbol"]);
groups.boxedPrimitives = new Set(["boolean", "number", "string", "symbol"]);
groups.basicNatives = new Set(["pojo", "array", "function", "date", "regexp"]);
groups.es6Natives = new Set(["map", "set", "weakmap", "weakset"]);
groups.dataset = new Set(["pojo", "array", "map", "set", "weakmap", "weakset"]);
groups.staticNatives = new Set(["Math", "Reflect", "JSON"]);
groups.typedArrays = new Set(["DataView", "ArrayBuffer"]);
groups.acceptedArgs = new Set(["pojo", "array", "boolean", "number", "string"]);
groups.iterables = new Set(["string", "array", "map", "set"]);


const littleEndian = new Uint32Array((new Uint8Array([1, 2, 3, 4])).buffer)[0] === 0x04030201; // true if LE
