/**
 * Identify the type of value.
 * @param   {*}  x    Any value.
 * @returns {string}  Type of value.
 */
const id = function (x) {
    'use strict';
    let typ = typeof (x);
    let tos = Object.prototype.toString.call(x).slice(8, -1).toLowerCase();

    //* undefined
    if (tos === "undefined") return "undefined";

    //* null
    if (tos === "null") return "null";

    //* boolean
    if (tos === "boolean") return "boolean";

    //* string
    if (tos === "string") return "string";

    //* symbol
    if (tos === "symbol") return "symbol";

    //* number
    if (tos === "number") {
         //* NaN
        if (Number.isNaN(x)) return "NaN";
        // integer: int, uint, unsafe
        if (Number.isFinite(x)) {
            //* int
            if (Number.isInteger(x)) return "int";
            //* float
            if (Number.isFinite(x) && (Math.abs(x % 1) !== 0)) return "float";
        }
        return tos;
    }

    //* function
    if (tos === "function") return "function";

    //* array
    if (tos === "array") return "array";

    //* pojo
    if (tos === "object") return "pojo";

    //! else:
    return tos;

};



//* ===================== EXPORT ====================
if (typeof module !== undefined) module.exports = id;

