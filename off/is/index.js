'use strict';

let tos = (x) => Object.prototype.toString.call(x);

const is = {

  //* ============================ PRIMITIVES ==================================
    primitive: (x) => (typeof x !== 'object' && typeof x !== 'function'),
    undef: (x) => (typeof x === 'undefined'),
    nil: (x) => (tos(x) === "[object Null]"),
    boolean: (x) => (tos(x) === "[object Boolean]"),
    symbol: (x) => (tos(x) === "[object Symbol]"),

  //* ============================== STRINGS ====================================
    string: (x) => (tos(x) === "[object String]"),
    alpha: (x) => /\w+/.test(x),

  //* ============================== NUMBERS ====================================
    number: (x) => (tos(x) === "[object Number]"),
    float: (x) => (Number.isFinite(x) && (Math.abs(x % 1) !== 0)),
    // integers and floats (without NaN, Infinity, -Infinity)
    numeric: (x) => (typeof x === 'number' && Number.isFinite(x)),
    // integers and floats as strings
    numericString: (x) => (typeof x === 'string' && Number.isFinite(Number(x))),
    integer: (x) => Number.isInteger(x),
    safeInt: (x) => Number.isSafeInteger(Number(x)),
    unsignedInteger: (x) => (Number.isInteger(x) && x > -1),

  //* =============================== OBJECTS ===================================
    object: (x) => (typeof x === "object" || typeof x === "function"),
    map: (x) => (tos(x) === "[object Map]"),
    set: (x) => (tos(x) === "[object Set]"),
    func: (x) => (typeof x === "function"),

   //! =============================== ARRAYS ====================================
    array: (x) => (Array.isArray(x)),
    arrayLike: (x) => (typeof x === "object" && "length" in Reflect.ownKeys(x)),
    arrayIndex: (x) => /^0$|^[1-9]\d*$/.test(x) && x <= 4294967294, // 2^32-2
    nestedArray: (x) => x.some(el => Array.isArray(el)),

    //! =============================== POJO =====================================
    pojo(x) {
        if (typeof x !== "object") return false;
        if (tos(x) !== "[object Object]") return false;
        if (Reflect.ownKeys(x).includes("length")) return false;
        if (Reflect.ownKeys(x).includes("size")) return false;
        return true;
    },

  //* ============================= EMPTY =====================================

    //! ------------------ emptyArray -------------------
    emptyArray(x) {
        return (this.array(x) && x.length === 0);
    },

    //! ------------------ emptyPojo --------------------
    emptyPojo(x) {
        return (this.pojo(x) && Reflect.ownKeys(x).length === 0);
    },

    //! ------------------ emptyMap --------------------
    emptyMap(x) {
        return (this.map(x) && this.size === 0);
    },

    //! ------------------ emptySet --------------------
    emptySet(x) {
        return (this.set(x) && this.size === 0);
    },

    //! ============================= EMPTY =====================================
    /*
     * Empty values:
     *  1) undefined
     *  2) null
     *  3) string:      ""
     *  4) number:      NaN, Infinity
     *  5) array:       []
     *  6) pojo:        {}
     *  7) map
     *  8) set
     *  *) function:    (() => void 0)   or   function(){}
     */
    empty(x) {
        // 1) undefined
        if (this.undef(x)) return true;
        // 2) null
        if (this.nil(x)) return true;
        // 3) "" (string)
        if (this.string(x) && x.length === 0) return true;
        // 4) NaN, Infinity (number)
        if (!this.numeric(x)) return true;
        // 5) [] (array)
        if (this.array(x)) return this.emptyArray(x);
        // 6) {} (pojo)
        if (this.pojo(x)) return this.emptyPojo(x);
        // 7) map
        if (this.map(x)) return this.emptyMap(x);
        // 8) set
        if (this.set(x)) return this.emptySet(x);

        // otherwise
        return false;
    },

    //* ============================= MISC =====================================
    nothing (x) {
        if ([null, undefined, "", NaN].includes(x)) return true;
        return false;
    },


}; // end is



//* ================ ALIASES ================
is.o = is.obj = is.object;
is.a = is.arr = is.array;
// is.arrayLike
is.intidx = is.intIndex = is.integerIndex = is.arrayIndex;
is.f = is.fn  = is.fx = is.fun = is.func;
is.p = is.pojo;

is.m = is.prim  = is.primitive;
is.z = is.nill  = is.nil;
is.u = is.void  = is.undef;
is.b = is.bool  = is.boolean;
is.s = is.str   = is.string;
is.y = is.sym   = is.symb = is.symbol;

is.n = is.nr    = is.num  = is.number;
is.N = is.numeric
is.nstr = is.nstring = is.numstr = is.numericString

is.i = is.int   = is.integer;
is.U = is.uint  = is.unsignedInteger;
is.F = is.float;


//* ================ EXPORT =================
if (typeof module !== "undefined") module.exports = is;


is.alpha("red"); //?
is.alpha("red dead6"); //?

var a = "65.9";
/^\d+$/.test(a); //?

var alpha = `8-8 police's, åloΣúgo "and" x_fire `;
/^[\d\w ,"'-]+$/m.test(alpha); //?