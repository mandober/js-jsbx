/**
 * Determine if value is an array.
 *
 * @param    {any}      x   Any value.
 * @returns  {boolean}  Returns `true` if value is an array, `false` otherwise.
 */
module.exports = function isArray(x) {
    'use strict';
    try {
        if (Array.isArray(x)) return 1;
    } catch (e) {
        //console.log(e);
        if (Object.prototype.toString.call(x) === '[object Array]') return 1;
    }
     
    

    if (typeof x === "object" && "length" in Reflect.ownKeys(x)) return true;
    return false;


    
    return (Array.isArray)
        ? (Array.isArray(x))
        : (Object.prototype.toString.call(x) === '[object Array]');
}


