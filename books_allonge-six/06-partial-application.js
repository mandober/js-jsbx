//! partial application

// When a function takes multiple arguments, we apply the function to the arguments by evaluating it with
// all of the arguments, producing a value. But what if we only supply some of the arguments? In that case,
// we can’t get the final value, but we can get a function that represents part of our application.

// This code implements a partial application of the map function by applying the function (n) => n * n as its second argument. The resulting function�squareAll�is still the map function, it�s just that we�ve applied one of its two arguments already.

//* partial function application refers to the process of fixing a number of arguments to a function, producing another function of smaller arity.

const map = (a, fn) => a.map(fn);

const squareAll1 = (array) => map(array, (n) => n * n);

// mapWith takes any function as an argument and returns a partially applied map function.
const mapWith = (fn) => (array) => map(array, fn);

const squareAll = mapWith((n) => n * n);

squareAll([1, 2, 3]); /*?*/
//=> [1, 4, 9]

// The important thing to see is that partial application is orthogonal to composition, and that they both work together nicely:
const maybe = (fn) =>
    function (...args) {
        if (args.length === 0) {
            return
        }
        else {
            for (let arg of args) {
                if (arg == null) return;
            }
            return fn.apply(this, args)
        }
    };

const safeSquareAll = mapWith(maybe((n) => n * n));

safeSquareAll([1, null, 2, 3]); /*?*/
//=> [1, null, 4, 9]




// These two recipes are for quickly and simply applying a single argument, either the leftmost or rightmost
// This partial recipe allows us to create functions that are partial applications of functions that are
//* context aware.
// We�d need a different recipe if we wish to create partial applications of object methods.

const callFirst = (fn, larg) =>
    function (...rest) {
        return fn.call(this, larg, ...rest); //* context awareness
    };

const callLast = (fn, rarg) =>
    function (...rest) {
        return fn.call(this, ...rest, rarg);
    };

const greet = (me, you) => `Hello, ${you}, my name is ${me}`;

const heliosSaysHello = callFirst(greet, 'Helios');
heliosSaysHello('Eartha'); /*?*/

const sayHelloToCeline = callLast(greet, 'Helios');
sayHelloToCeline('Eartha'); /*?*/



// We take it a step further, and can use gathering and spreading to allow
// for partial application with more than one argument:
const left = (fn, ...args) =>
    (...remainingArgs) =>
        fn(...args, ...remainingArgs);

const right = (fn, ...args) =>
    (...remainingArgs) =>
        fn(...remainingArgs, ...args);

const hi = (me, ...you) => `Hello, ${you}, my name is ${me}`;

const hiJoe = left(hi, 'joe', 'john');
hiJoe('nick', 'jane', 'sam'); /*?*/

const toJoe = right(hi, 'joe', 'jim');
toJoe('nick', 'jane', 'sam'); /*?*/


//! Unary
// Unary is a function decorator that modifies the number of arguments a function takes:
// Unary takes any function and turns it into a function taking exactly one argument.
const unary = (fn) =>
    fn.length === 1
        ? fn
        : function (something) {
            return fn.call(this, something)
        };

// example usage: parseInt has an optional 2nd arg (radix) so using it as a callback to map
// an array of stringified numbers is a no go...unless we set it to take only 1 arg
['1', '2', '3'].map(unary(parseInt));
//=> [1, 2, 3]
