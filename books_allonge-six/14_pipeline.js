// compose is extremely handy, but one thing it doesnt communicate well is the order on operations.
var compose = (...fns) =>
    (value) =>
        fns.reverse().reduce((acc, fn) => fn(acc), value);

//! pipeline
const pipeline = (...fns) =>
    (value) =>
        fns.reduce((acc, fn) => fn(acc), value);


// pipeline says �add one to the number and then double it�
var setter2 = pipeline(addOne, double);
// compose says, �double the result of adding one to the number�
var setter1 = compose(addOne, double);
// Both do the same job, but communicate their intention in opposite ways.

