// a function that either takes functions as arguments, or returns a function, or both,
//* is referred to as a higher-order function.

const repeat = (num, fn) =>
    (num > 0)
        ? (repeat(--num, fn), fn(num))
        : undefined;

repeat(3, function (n) {
    console.log(`Hello ${n}`);
}); /*?*/


//! A combinator is a higher-order function that uses only function application and earlier defined combinators to define a result from its arguments.
// - Wikipedia

//* Compose or "B combinator" or "Bluebird".
// Here is the typical15 programming implementation:
const compose = (a, b) => (c) => a(b(c));

