//! Tap
// One of the most basic combinators is the "K Combinator" nicknamed the "Kestrel"
const K = (x) => (y) => x;

// it can be used when you want to do something with a value for side-effects, but keep the value around:
const tap2 =
        (value) =>
            (fn) =>
                (typeof fn === 'function') && (fn(value), value);

//? It takes a value and returns a function that always returns that value,
//? but if you pass it a function, it executes the function for side-effects.

// usage: poor-man’s debugger:
tap2('espresso')((it) => {
    console.log(`Our drink is '${it}'`)
}); /*?*/
//=> Our drink is 'espresso'
//=> 'espresso'

// It’s easy to turn off:
tap2('espresso')(); /*?*/
//=> 'espresso'


//? Libraries like Underscore use a version of tap that is "uncurried"
//_.tap('espresso', (it) => console.log(`Our drink is '${it}'`));
//=> Our drink is 'espresso'
//=> 'espresso'


//! Let’s enhance our recipe so that it works both ways:
const tap = (value, fn) => {
    const curried = (fn) => typeof fn === "function" && (fn(value), value);
    return fn === undefined ? curried : curried(fn);
};

// Now we can write:
tap('espresso')(it =>
    console.log(`Our drink is '${it}'`)); /*?*/
//=> Our drink is 'espresso'
//=> 'espresso'

// Or:

tap('espresso', (it) =>
    console.log(`Our drink is '${it}'`)); /*?*/
//=> Our drink is 'espresso'
//=> 'espresso'

// And if we wish it to do nothing at all, We can write either
tap('espresso')(); /*?*/
// or
tap('espresso', null); /*?*/
