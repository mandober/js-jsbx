//? unary
const unary = (fn) =>
    fn.length === 1
        ? fn
        : (x) => fn(x);

//? unary2 (`this` aware unary)
const unary2 = (fn) =>
    fn.length === 1
        ? fn
        : function (something) {
            return fn.call(this, something)
          };


//* identity (or "I combinator" or "Idiot Bird")
const identity = (x) => (x);

//* constant function maker (or "K combinator" or "Kestrel")
const kestrel = (x) => (y) => x;

//* compose (or the B Combinator or "Bluebird")
var bluebird = (f, g) => (x) => f(g(x));

//* "V combinator" or "Vireo"
const vireo = (x) => (y) => (f) => f(x)(y);


//? not
const not = (fn) => (x) => !fn(x);

//? maybe
const maybe = (fn) =>
    function (...args) {
        if (args.length === 0) return;
        for (let arg of args) {
            if (arg == null) return;
        }
        return fn.apply(this, args);
    };

//? once
const once = (fn) => {
    let done = false;
    return function () {
        return done ? void 0 : ((done = true), fn.apply(this, arguments))
    }
};


//? map
const map = (arr, fn) => arr.map(fn);


//? mapWith
// takes any function as an argument and returns a partially applied map function.
const mapWith = (fn) => (arr) => map(arr, fn);


//? tap2
const tap2 = (value) =>
                    (fn) => (typeof (fn) === 'function' && fn(value), value)

//? tap
// `tap` that works both ways (curried and uncurried)
const tap = (value, fn) => {
    const curried = (fn) => (typeof (fn) === 'function' && fn(value), value);
    return fn === undefined ? curried : curried(fn);
}


//? compose (or the B Combinator)
const B = (f, g) => (x) => f(g(x));

//? variadic recursive compose
const compose2 = (f, ...fx) =>
    fx.length === 0
        ? f
        : (x) => f(compose(...fx)(x));

//? variadic compose
const compose = (...fx) =>
    (x) => fx.reverse()
        .reduce((acc, f) => f(acc), x);
