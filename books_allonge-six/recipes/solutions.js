// solutions

//? repeat
// recursive function to repeat a function n times
const repeat = (num, fn) => (num > 0) ? (repeat(--num, fn), fn(num)) : undefined;
repeat(3, (n) => console.log(`Hello ${n}`)); /*?*/


//? even
// recursively calculate if number is even:
const even = (x) => (x === 0) ? true : !even(--x);


// recursively calculate if number is even with more complication
const evens = (n) => {
    const even = (x) => {
        if (x === 0)
            return true;
        else {
            const odd = (y) => !even(y);
            return odd(x - 1);
        }
    }
    return even(n);
};
