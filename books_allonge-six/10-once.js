//! once

// once is an extremely helpful combinator.It ensures that a function can only be called, well, once.Here’s the recipe:
const once = (fn) => {
    let done = false;

    return function () {
        return done ? void 0 : ((done = true), fn.apply(this, arguments))
    }
};

const askedOnBlindDate = once(
    () => "sure, why not?"
);

askedOnBlindDate()
//=> 'sure, why not?'

askedOnBlindDate()
//=> undefined

