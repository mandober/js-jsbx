//! Functions

// since variables used in a function are either bound or free, we can
// bifurcate functions into those with free variables and those without:
//? Functions containing no free variables are called pure functions.
//? Functions containing one or more free variables are called closures.

//? pure functions
(() => {});
((x) => x);
((x) => (y) => x);

// from the 3rd example we see that
//? a pure function can contain a closure.

((x) => x);
// is called the I Combinator, or the Identity Function.

((x) => (y) => x);
// is called the K Combinator, or Kestrel



//! Naming

// In order to bind 3.14159265 to the name PI, we’ll need a function
// with a parameter of PI applied to an argument of 3.14159265.
((PI) => (diameter) => diameter * PI)(3.14159265);

// This expression, when evaluated, returns a function that calculates circumferences
((diameter) => diameter * 3.14159265)(2); //=> 6.2831853

((PI) => (diameter) => diameter * PI)(3.14159265)(2);
//=> 6.2831853

// That works! We can bind anything we want in an expression by wrapping it
// in a function that is immediately invoked with the value we want to bind

// There’s another way we can make a function that binds 3.14159265 to the name PI
// and then uses that in its expression. We can turn things inside-out by putting the
// binding inside our diameter calculating function, like this:
((diameter) => ((PI) => diameter * PI)(3.14159265));

// It produces the same result as our previous expressions for a diameter-calculating function:

((diameter) => diameter * 3.14159265)(2); //=> 6.2831853
// bad: it has a "magic number"

((PI) => (diameter) => diameter * PI)(3.14159265)(2); //=> 6.2831853
// better: Exposes naming PI first, but we have to look inside to find out why we care

((diameter) => ((PI) => diameter * PI)(3.14159265))(2); //=> 6.2831853
// good: it separates concerns nicely; the outer function describes its parameters


// So, should we should always write this:
((diameter) => ((PI) => diameter * PI)(3.14159265));
// well, the wrinkle with this is that typically, invoking functions is considerably more expensive than
// evaluating expressions: Every time we invoke the outer function, we’ll invoke the inner function.
// We could get around this by writing:
((PI) => (diameter) => diameter * PI)(3.14159265);
// But then we’ve obfuscated our code, and we don’t want to do that unless we absolutely have to.
// What would be very nice is if the language gave us a way to bind names inside of blocks without
// incurring the cost of a function invocation. And JavaScript does.



//! const
// Another way to write our “circumference” function would be to pass PI along
// with the diameter argument, something like this:
((diameter, PI) => diameter * PI);
// And we could use it like this:
((diameter, PI) => diameter * PI)(2, 3.14159265); //=> 6.2831853
// This differs from our example above in that there is only one environment, rather than two.
// We have one binding in the environment representing our regular argument, and another our constant.
// That’s more efficient, and it’s almost what we wanted all along: A way to bind 3.14159265 to a readable name.

// The const keyword introduces one or more bindings in the block that encloses it.
// It doesn’t incur the cost of a function invocation.
((diameter) => {
    const PI = 3.14159265;
    return diameter * PI;
});
// Even better, it puts the symbol (like PI) close to the value (3.14159265).

// It works just as we want:
// Instead of:
((diameter) =>
    ((PI) =>
        diameter * PI)(3.14159265))(2)
// Or:
((diameter, PI) => diameter * PI)(2, 3.14159265)
// We write:
((diameter) => {
    const PI = 3.14159265;
    return diameter * PI;
})(2);


// We can bind any expression.Functions are expressions, so we can bind helper functions:
((d) => {
    const calc = (diameter) => {
        const PI = 3.14159265;
        return diameter * PI;
    };
    return "The circumference is " + calc(d)
});


//! nested blocks

((n) => {
    const even = (x) => {
        if (x === 0)
            return true;
        else
            return !even(x - 1);
    }
    return even(n)
})(13);
//=> false

const even = (x) => (x === 0) ? true : !even(--x);


// And this also works:
((n) => {
    const even = (x) => {
        if (x === 0)
            return true;
        else {
            const odd = (y) => !even(y);
            return odd(x - 1);
        }
    }
    return even(n);
})(42);
//=> true

// We’ve used a block as the else clause, and since it’s a block, we’ve placed a const statement inside it.
