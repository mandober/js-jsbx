//! left-variadic decorator

const leftGather = (outputArrayLength) => {
    return function (inputArray) {
        return [inputArray.slice(0, inputArray.length - outputArrayLength + 1)].concat(
            inputArray.slice(inputArray.length - outputArrayLength + 1)
        )
    }
};

const [butLast, last] = leftGather(2)(['why', 'hello', 'there', 'little', 'droid']);

butLast;
//=> ['why', 'hello', 'there', 'little']

last;
//=> 'droid'
