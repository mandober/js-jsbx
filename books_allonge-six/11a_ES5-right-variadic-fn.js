//? ES5: Variadic Functions

/*
//? ES5: Right-variadic function
Right-variadic functions come easy in ES6 thanx to spread operator, but how to achieve the same thing in ES5?

So we have a fn that takes has 2 params. It needs to:
- gather 1st arg in 1. declared param (which it does anyway, by default)
- but also gather the rest of args in 2.param, as an array
Special cases:
- if there's no args, return undefined AND empty array
- if there's only 1 arg, return that arg AND empty array
*/
function es5rv1(first, rest) {
    first;
    rest;
    var argc = arguments.length; /*?*/
    var argv = Array.prototype.slice.call(arguments); /*?*/
    rest = argv.slice(1); /*?*/
    return [first, rest]; /*?*/
}

var [uno, due] = es5rv1(5, 6, 7); /*?*/
uno;
due;


/*
Ok, so this works for this particular fn and only for 2 declared params.
What if we need a fn that can gather params just like es6 does.
Namely, to gather individual args in declarted params,
but gather the rest of args in the last declared param.

//! ES5 Right-Variadic Function Decorator (RVFD)
So we need to make a Right-Variadic Function Decorator (RVFD) that:
- takes a plain fn and decorates it, giving it the ability to
- puts args in declarted parameters (as usual) but also to
- gather the excessive args in the last declared param
- returns a decorated function
*/
function rvfd(fn) {
    // slice shorthand
    var $slice = Array.prototype.slice;
    //var args = (arguments.length === 1 ? [arguments[0]] : Array.apply(null, arguments));

    // src fn: num of declared params
    var argc = fn.length; /*?*/

    // check arguments array-like obj:
    arguments;
    // as expeced, it shows: { 0:λ }

    // otherwise, return a new fn
    return function () {

        // check arguments array-like obj now:
        arguments;
        // { 0:0, 1:1, 2:2}
        //! FTW?!
        // How does it now gains access to calling args of src fn?!


        var ordinaryArgs = (arguments.length >= 1)
            ? $slice.call(arguments, 0, fn.length - 1) : [];

        var restOfTheArgsList = $slice.call(arguments, fn.length - 1);

        var args = (fn.length <= arguments.length)
            ? ordinaryArgs.concat([restOfTheArgsList]) : [];

        return fn.apply(this, args);

    }

};


//* this plain es5 fn
var param2 = function(first, rest) {
    return [first, rest];
};

//* after decorating it with rvfd:
var param2Dec = rvfd(param2);

//* is expected to return a new decorated fn, that when called:
param2Dec(0, 1, 2, 3); /*?*/
// is expected to return
// [ [0], [1, 2, 3] ]











/*
this plain es5 fn
var es5param3 = function (first, second, rest) {
    return rest;
};
// after decorating it with rvfd, is expected
// to return an array of excessive params:
// a, b, [c, d, e, ...]
*/
