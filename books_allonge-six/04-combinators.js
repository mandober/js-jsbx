//! COMBINATORS
// https://leanpub.com/javascriptallongesix/read#combinators

//? A combinator is a higher-order function that uses only function application
//? and earlier defined combinators to define a result from its arguments.
// - Wikipedia



//! Compose function or "B combinator" or "Bluebird"
// typical implementation:
const compose = (a, b) => (c) => a(b(c));

// if we have:
const addOne = (number) => number + 1;
const doubleOf = (number) => number * 2;
// With compose, anywhere you would write
const doubleOfAddOne = (number) => doubleOf(addOne(number));
// You could also write:
const doublePlusOne = compose(doubleOf, addOne);

// testing:
doubleOfAddOne(2); /*?*/
const plusOneDouble = compose(addOne, doubleOf);
doublePlusOne(2); /*?*/
plusOneDouble(2); /*?*/



//! Identity function or "I combinator" or "Idiot Bird"
// it evaluates to whatever parameter you pass it.
var I = (x) => (x);
I(1); /*?*/



//! Constant function maker or "K combinator" or "Kestrel"
var K = (x) => (y) => x;

//? A constant function is a function that always returns the same thing, no matter what you give it.
// For example, (x) => 42 is a constant function that always evaluates to 42.

// The kestrel, or K, is a function that makes constant functions.
// You give it a value, and it returns a constant function that gives that value.
// Given two values, we can say that K always returns the first value: K(x)(y) => x
K(1)(2); /*?*/


// K(I)
K(I)(1)(2) /*?*/

var K2 = K(I); /*?*/


var V = (x) => (y) => (a) => a(x)(y);
var B = (a, b) => (x) => a(b(x));
