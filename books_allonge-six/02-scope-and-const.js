// const and lexical scope

((fn) =>
    fn(2))(
    ((PI) =>
        (diameter) =>
            diameter * PI)
                (3.14159265)
); /*?*/
//=> 6.2831853

// which is -->
((fn) => fn(2))
    ((PI) => (diameter) => diameter * PI)
        (3.14159265); /*?*/

// which is -->
((fn) => fn(2))
    ((diameter) => diameter * 3.14159265); /*?*/

// which is -->
(diameter => diameter * 3.14159265)(2); /*?*/


// closures and rebinding
// Although we will bound 3 to PI in the environment surrounding fn(2), the value that counts
// is 3.14159265, the value we bound to PI in the environment surrounding (diameter) ⇒ diameter * PI.
((fn) =>
    ((PI) =>
        fn(2)
    )(3)
)(
    ((PI) =>
        (diameter) =>
            diameter * PI)
                (3.14159265)
); /*?*/


// const and rebinding
((fn) => {
    const PI = 3;
    return fn(2);
})(
    (() => {
        const PI = 3.14159265;
        return (diameter) => diameter * PI;
    })()
); /*?*/


//? So where are const variables bound? In the function environment?
//? Or in an environment corresponding to the block?

// We can test this by creating another conflict. But instead of binding two different variables to the same name in two different places, we’ll bind two different values to the same name, but one environment will be completely enclosed by the other.

// Let’s start, as above, by doing this with parameters.We’ll start with:
    ((PI) =>
        (diameter) => diameter * PI
    )(3.14159265); /*?*/

// And gratuitously wrap it in another IIFE so that we can bind PI to something else:
((PI) =>
    ((PI) =>
        (diameter) => diameter * PI
    )(3.14159265) /*?*/
)(3); /*?*/

// This still evaluates to a function that calculates diameters:
((PI) =>
    ((PI) =>
        (diameter) => diameter * PI
    )(3.14159265) /*?*/
)(3)(2); /*?*/
//=> 6.2831853

// And we can see that our diameter*PI expression uses the binding for PI in the closest parent environment.
//? But one question: Did binding 3.14159265 to PI somehow change the binding in the “outer” environment?

// Let’s rewrite things slightly differently:
((PI) => {
    ((PI) => { })(3);
    return (diameter) => diameter * PI;
})(3.14159265); /*?*/

// Now we bind 3 to PI in an otherwise empty IIFE inside of our IIFE that binds 3.14159265 to PI.
//? Does that binding “overwrite” the outer one? Will our function return 6 or 6.2831853?
// the answer is no, the inner binding does not overwrite the outer binding.
//* the inner binding shadows the outer binding
((PI) => {
    ((PI) => { })(3);
    return (diameter) => diameter * PI;
})(3.14159265)(2); /*?*/
//=> 6.2831853


//? So what about const. Does it work the same way?
//* Yes, names bound with const shadow enclosing bindings just like parameters
((diameter) => {
    const PI = 3.14159265;

    (() => {
        const PI = 3;
    })();

    return diameter * PI;
})(2); /*?*/
//=> 6.2831853


// But wait! There’s more!!!
// Parameters are only bound when we invoke a function. That’s why we made all these IIFEs.
// But const statements can appear inside blocks.
//? What happens when we use a const inside of a block?

// Let’s try it:
((diameter) => {
    const PI = 3;
    if (true) {
        const PI = 3.14159265;
        return diameter * PI;
    }
})(2); /*?*/
//=> 6.2831853

((diameter) => {
    const PI = 3.14159265;
    if (true) {
        const PI = 3;
    }
    return diameter * PI;
})(2); /*?*/
//=> 6.2831853

//* Ah! const statements don’t just shadow values bound within the environments created by functions,
//* they shadow values bound within environments created by blocks!
