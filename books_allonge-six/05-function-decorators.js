//! function decorators

//! A function decorator is a higher-order function that takes one function as an argument,
//! returns another function, and the returned function is a variation of the argument function.

// Here is a ridiculously simple decorator:
const not = (fn) => (x) => !fn(x);

// So instead of writing !someFunction(42), we can write not(someFunction)(42).
// Hardly progress. But like compose, we could write either:
const something = (x) => x !== null;
// And elsewhere, write:
const nothing = (x) => !something(x);
// Or we could write:
const nothing2 = not(something);


//? once
// decorator called "once": It ensures that a function can only be executed once. Thereafter, it does nothing.
// Once is useful for ensuring that certain side effects are not repeated.

//? maybe
// It ensures that a function does nothing if it is given nothing (like null or undefined) as an argument.

