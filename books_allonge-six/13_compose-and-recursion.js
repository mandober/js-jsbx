//! variadic compose and recursion

// Here is the B Combinator, or compose
var compose = (f, g) => (x) => f(g(x));

// If we wanted to implement a compose3, we could write:
var compose3 = (f, g, h) => (x) => f(g(h(x)));

// Or observe that it is really:
var compose3 = (f, g, h) => compose( f( compose(g, h) ) );

// Once we get to compose4, we ask ourselves if there is a better way.
// For example, if we had a variadic compose, we could write
// `compose(a, b)`, `compose(a, b, c)`, or `compose(a, b, c, d)`

//* We can implement a variadic compose recursively.
// The easiest way to reason about writing a recursive compose is to start with
// the smallest or degenerate case:
// If compose only took one argument, it would look like this:
var compose = (f) => f;
// (take a function and return that function)

// The next thing is to have a way of breaking a piece off the problem.
// We can do this with a variadic function:
var compose = (f, ...fn)
    => "to be determined";

// We can test whether we have the degenerate case ( `(f) => f` ):
var compose = (f, ...fn) => //? breaking a piece off the problem
    fn.length === 0
        ? f //? degenerate case
        : "to be determined"

// If it is not the degenerate case, we need to combine what we have with the solution for the rest.
// In other words, we need to combine `fn` with `compose(...rest)` How do we do that?

// Well, consider `compose(f, g)`. We know that `compose(f)` is the degenerate case, it's just `f`:
//* compose = (f) => f;
// And we know that
//* compose(f, g) === (x) => f(g(x));
// So let's substitute 'g' with `compose(g)`:
//* compose(f, g) === (x) => f(compose(g)(x));
// Now substitute `g` with `...fx`:
//* compose(f, ...fx) === (x) => f(compose(...fx)(x));

// This is our solution:
//! variadic recursive compose
var compose = (f, ...fx) =>
    fx.length === 0
        ? f
        : (x) => f( compose(...fx)(x) );

// There are others, of course.
// compose can be implemented with iteration or with `reduce`, like this:
var compose = (...fx) =>
    (x) => fx.reverse()
             .reduce((acc, f) => f(acc), x);
