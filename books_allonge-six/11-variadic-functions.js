'use strit';

//! Variadic Functions

const variadic = function(...args) {
    args;
}(1,2,3);

// A variadic function is a function that is designed to accept a variable number of arguments.
// In es6, you can make a right-variadic function by gathering parameters. For example:
const rv = (first, ...rest) => {
    first;
    rest;
    return [first, rest];
};
rv(); // undefined, []
rv(1); // 1, []
rv(1, 2); // 1, [2]
rv(1, 2, 3); // 1, [2,3]


//! Right-variadic ES5 Polyfill
function rightVariadic(FxPlain) {
    'use strit';
    // shorthand
    var $slice = Array.prototype.slice;

    // if src (FxPlain) has no params, return it
    if (FxPlain.length === 0) return FxPlain;

    // otherwise, return a new FxPlain
    return function () {
        // arguments here is decorated FxPlain arguments object
        arguments;

        var FxDecorated_argc = arguments.length; /*?*/
        var FxPlain_argc = FxPlain.length; /*?*/
        var FxPlain_argLast = FxPlain_argc - 1; /*?*/

        // declaredArgs contains array of 1:1 declared args
        var declaredArgs = (arguments.length >= 1)
            // slice(0, 1)
            ? $slice.call(arguments, 0, FxPlain.length - 1) /*?*/
            : []; /*?*/

        // restArgs contains array of the rest of args
        var restArgs = $slice.call(arguments, FxPlain.length - 1); /*?*/

        var args = (FxPlain.length <= arguments.length)
            ? declaredArgs.concat([restArgs])
            : [];

        return FxPlain.apply(this, args);
    }
};

// fn plain has 2 params
function plain(first, rest) {
    return [first, rest];
}

// make a new fn called `decorated()`, by decorating `plain()` fn with `RVFD()`
var decorated = rightVariadic(plain);
decorated.length; /*?*/

// call `decorated()` with 4 args
decorated(1, 2, 3, 4); /*?*/
// decorated('a', 'b'); /*?*/
// decorated('a'); /*?*/
// decorated(); /*?*/




//! right-variadic
// We dont need rightVariadic any more, because we now simply write:
const firstAndTail = (first, ...rest) => [first, rest];

var [first, rest] = firstAndTail(1, 2, 3, 4); /*?*/
first;
rest;

firstAndTail(1, 2); /*?*/
firstAndTail(1); /*?*/
firstAndTail(); /*?*/

// This is a right-variadic function, meaning that it has one or more fixed arguments,
// and the rest are gathered into the rightmost argument.


//? left-variadic
// All left-variadic functions have one or more fixed arguments,
// and the rest are gathered into the leftmost argument

//! left-variadic decorator
function leftVariadic(fn) {
    if (fn.length === 0) return fn;
    return function () {
        var ordinaryArgs = (arguments.length >= 1) ? $slice.call(arguments, 0, fn.length - 1) : [];
        var restOfTheArgsList = $slice.call(arguments, fn.length - 1);
        var args = (fn.length <= arguments.length) ? ordinaryArgs.concat([restOfTheArgsList]) : [];
        return fn.apply(this, args);
    }
};

var headAndLast = leftVariadic(
    function (head, last) {
        return [head, last];
    });


//var [head, last] =
headAndLast('why', 'hello', 'there', 'little', 'droid');
//=> [ ["why","hello","there","little"], "droid" ]
// head; // ["why","hello","there","little"]
// last; // "droid"
