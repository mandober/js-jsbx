//! variadic recursive compose
var compose2 = (f, ...fx) =>
    fx.length === 0
        ? f
        : (x) => f(compose(...fx)(x));

//! variadic compose
const compose = (...fx) =>
    (x) => fx.reverse()
        .reduce((acc, f) => f(acc), x);



// TEST
var s = [9, 7, 6, 4, 8];

var f = (arr) => arr.sort();
var g = (arr) => arr.map((el) => el * 2);
var h = (arr) => arr.reverse();
var k = (arr) => arr.map((el) => el - 3);

s; //?

f(s); //?
compose(f)(s); //?
compose2(f)(s); //?

g(f(s)); //?
compose(g, f)(s); //?
compose2(g, f)(s); //?

h(g(f(s))); //?
compose(h, g, f)(s); //?
compose2(h, g, f)(s); //?

k(h(g(f(s)))); //?
compose(k, h, g, f)(s); //?
compose2(k, h, g, f)(s); //?

