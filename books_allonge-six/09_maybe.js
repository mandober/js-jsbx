//! maybe
//* It ensures that a function does nothing if given nothing (like null or undefined) as an argument.

// A common problem in programming is checking for null or undefined
// hereafter called “nothing” while all other values including 0, [] and false will be called “something”

// This recipe concerns a pattern that is very common: A function fn takes a value as a parameter, and its behaviour by design is to do nothing if the parameter is nothing:
const isNotEmpty = (value) =>
    value !== null && value !== void 0;

const checksForSomething = (value) => {
    if (isNotEmpty(value)) {
        // function's true logic
    }
};

// Alternately, the function may be intended to work with any value, but the code calling the function wishes to emulate the behaviour of doing nothing by design when given nothing:
var value = 0;
var something =
    isNotEmpty(value)
        ? console.log(value)
        : value;


//! maybe
const maybe = (fn) =>
    function (...args) {
        if (args.length === 0) return;

        for (let arg of args) {
            if (arg == null) return;
        }
        return fn.apply(this, args);
    };

// maybe reduces the logic of checking for nothing to a function call:

maybe((a, b, c) => a + b + c)(1, 2, 3); /*?*/
//=> 6

maybe((a, b, c) => a + b + c)(1, null, 3); /*?*/
//=> undefined

